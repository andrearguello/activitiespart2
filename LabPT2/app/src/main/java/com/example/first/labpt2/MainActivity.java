package com.example.first.labpt2;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.sql.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<ArrayItem> array;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        array = new ArrayList<>();
        array.add(new ArrayItem((int) 1,"First line", "Second line"));
        array.add(new ArrayItem((int) 2, "Third line", "Fourth line"));
        array.add(new ArrayItem((int) 3,"Fifth line", "Sixth line"));



        final ListView list= (ListView)findViewById(R.id.listViews);
        ArrayAdapter<ArrayItem> adapt = new ArrayAdapter<ArrayItem>(MainActivity.this, android.R.layout.simple_list_item_1, array);
        list.setAdapter(adapt);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int index=i; //Position in the list
                Object data = list.getItemAtPosition(index);
                Intent intent = new Intent(MainActivity.this, SecondView.class);
                intent.putExtra("ObjectToSend", (Parcelable) data);
                startActivity(intent);
            }
        });
    }
}
