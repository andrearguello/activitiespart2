package com.example.first.labpt2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class SecondView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_view);

        Intent intent = getIntent();
        ArrayItem example= intent.getParcelableExtra("ObjectToSend");

        int imageRes = example.getImagen();
        String text1=example.getT1();
        String tex2= example.getT2();


        TextView textView1 = findViewById(R.id.textView);
        String finalText = String.valueOf(imageRes) + " "+ text1+ " " +tex2;
        textView1.setText(finalText);

    }
}
