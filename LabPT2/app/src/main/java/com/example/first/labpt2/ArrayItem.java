package com.example.first.labpt2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by cooli on 22/02/2018.
 */

public class ArrayItem implements Parcelable {
    private int imagen;
    private String text1;
    private String text2;

    public ArrayItem(int resource, String texto1, String texto2){
        imagen=resource;
        text1=texto1;
        text2=texto2;
    }

    protected ArrayItem(Parcel in) {
        imagen = in.readInt();
        text1 = in.readString();
        text2 = in.readString();
    }

    public static final Creator<ArrayItem> CREATOR = new Creator<ArrayItem>() {
        @Override
        public ArrayItem createFromParcel(Parcel in) {
            return new ArrayItem(in);
        }

        @Override
        public ArrayItem[] newArray(int size) {
            return new ArrayItem[size];
        }
    };

    public int getImagen(){
        return imagen;
    }

    public String getT1(){
        return text1;
    }

    public String getT2(){
        return text2;
    }

    public String getAll(){
        String str=String.valueOf(getImagen());
        return str + " " + getT1() + " " + getT2();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(imagen);
        parcel.writeString(text1);
        parcel.writeString(text2);
    }
}
